#include "compare.h"
#include <stdlib.h>
#include "math.h"

void open_file(char *name, FILE **file, char *mode)
{
    *file = fopen(name, mode);
    if (*file == NULL)
    {
        fprintf(stderr, "error: file '%s' not found\n", name);
        exit(EXIT_FAILURE);
    }
}

int find_diff(FILE *first, FILE *second, struct Diff *diff)
{
    int steps = 0;
    char first_char, second_char;
    while ( (first_char = fgetc(first)) == (second_char = fgetc(second)) )
    {
        if (first_char == EOF || second_char == EOF)
        {
            diff->offset = -1;
            return 0;
        }
        steps ++;
    }
    diff->offset = steps;
    fseek(first, -1, SEEK_CUR);
    fseek(second, -1, SEEK_CUR);
    return 0;
}

int get_diff(FILE *first, FILE *second, int size, struct Diff *diff)
{
    char letter;
    for (int i = size-1; i >= 0; i --)
    {
        letter = fgetc(first);
        if (letter == EOF)
        {
            break;
        }
        diff->first_diff += (letter - '0') * (int) pow(2, i);
    }
    for (int i = size-1; i >= 0; i --)
    {
        letter = fgetc(second);
        if (letter == EOF)
        {
            break;
        }
        diff->second_diff += (letter - '0') * (int) pow(2, i);
    }
    return 0;
}

void print_hex(int num)
{
    printf("0x%x\n", num);
}
