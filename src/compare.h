/*
    File Comparison Write a C program to compare 2 binary files.
    It should take the 2 file names as command line parameters.
    It has to be a command line parameter and **not** accepting input from the user after the program is launched.
    Description:
        * Please complete the test within 4 days (Mar 8 -> Mar 11)
        * Write the code to be as professional as possible.
        * Handling all error cases.
        * Don’t use threading.
        * Pay attention to optimize reading from file and comparing.
        * Don’t use static or dynamic memory > 8K for buffers.
        * Measure the amount of time it would take to compare two
          1 MB files (which have some differences towards the end of the file).
            * The time should be measured within the program.
            * For modern processors, it shouldn't take more than 5 msec.
            * Some candidates don’t send these logs. This is important , please send the logs.
        * The program should print the offset at which the
          first difference is found and first 16 bytes (in hex) from
          the files starting at the point they start differing.

    Outputs:
        * Time benchmarks
        * Diff start offset
        * Next 16 bytes displayed as hex
*/
#ifndef COMPARE_H
#define COMPARE_H
#include <stdio.h>

struct Diff
{
    int offset;
    int first_diff;
    int second_diff;
};

void open_file(char *name, FILE **file, char *mode);
int find_diff(FILE *first, FILE *second, struct Diff *diff);
int get_diff(FILE *first, FILE *second, int size, struct Diff *diff);
void print_hex(int num);

#endif