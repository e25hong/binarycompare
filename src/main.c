#include "compare.h"
#include <stdlib.h>
#include <stdio.h>
#include "math.h"
#include "time.h"

int main(int argc, char *argv[]) {
    float start_time = (float) clock() / CLOCKS_PER_SEC;
    FILE *first;
    open_file("first.b", &first, "rb");
    FILE *second;
    open_file("second.b", &second, "rb");

    struct Diff *diff = (struct Diff *) malloc(sizeof(struct Diff));

    find_diff(first, second, diff);
    get_diff(first, second, 16, diff);

    printf("offset: %d\n", diff->offset);
    print_hex(diff->first_diff);
    print_hex(diff->second_diff);

    free(diff);
    fclose(first);
    fclose(second);

    float end_time = (float) clock() / CLOCKS_PER_SEC;
    printf("runtime: %f\n", end_time - start_time);
    return 0;
}
