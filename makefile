GXX = gcc
BUILD_DIR = build
SRC_DIR = src

build:
	cd build && $(GXX) ../$(SRC_DIR)/*.c -lm

run: build
	cd $(BUILD_DIR) && ./a.out first.b second.b

.PHONY: build
